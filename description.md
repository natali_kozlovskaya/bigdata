## Some calculations on "Big Data"-course (Yandex Data School)

  * [MapReduce](mr/README.md)

    Calculating of some metrics using MapReduce, the access to results are given from local-disk

  * [MapReduce and HBASE](mr_hbase/README.md)

    Calculating of some metrics using MapReduce (results are stored in HBASE-tables; responses to requests are given from HBASE-tables)

  * [Spark and Spark Streaming](spark/README.md)

    Calculating of the metric using Spark, calculating of the other using Spark Streaming.

  * [HiveQL](hiveql/README.md)

    Several scripts using HiveQL
