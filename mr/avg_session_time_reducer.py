#!/usr/bin/env python

import sys
import re
import datetime
import numpy as np


def main():
    
    sessions = [] # massive of all sessions' durations

    previous_ip = None
    start_session = None

    for line in sys.stdin:
        current_ip, date = line.split('\t', 1)
        current_time = datetime.datetime.strptime(date[:8], "%H:%M:%S")
        if current_ip == previous_ip:
            time = current_time - previous_time 
            if (time > datetime.timedelta(minutes=30)):
                # change session 
                # find difference between current time and time of sessions' begin 
                # write difference in massive
                all_session_time = previous_time - start_session
                sessions.append(all_session_time.seconds)
                start_session = current_time
        else:
            if start_session: # if not first iteration
                all_session_time = previous_time - start_session
                sessions.append(all_session_time.seconds)
            start_session = current_time
            previous_ip = current_ip
        previous_time = current_time

    # write last session
    all_session_time = previous_time - start_session
    sessions.append(all_session_time.seconds)

    sum = np.array(sessions).sum()
    length = len(sessions)
    print "%d\t%lf" % (length, float(sum))


if __name__ == '__main__':
    main()