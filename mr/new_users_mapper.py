#!/usr/bin/env python

import sys
import re
import datetime


def main():

    for line in sys.stdin:
        if (len(line.strip())):
            ip, date = line.strip().split('\t', 1)
            print "%s\t%d" % ('-'.join([ip, date]), 1)


if __name__ == '__main__':
    main()