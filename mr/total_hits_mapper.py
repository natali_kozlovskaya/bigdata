#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re

print >> sys.stderr, 'start mapper'
count=0
current_key=None
record_re = re.compile('([\d\.:]+) - - \[(\S+ [^"]+)\] "(\w+) ([^"]+) (HTTP/[\d\.]+)" (\d+) \d+ "([^"]+)" "([^"]+)"')

for line in sys.stdin:
	match = record_re.search(line)
	if not match:
		continue
	if match.group(6) != "200":
		continue
	count += 1
    
print "%s\t%d" % ('asd', count)
