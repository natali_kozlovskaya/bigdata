#!/usr/bin/env python

import sys

count = 0

for line in sys.stdin:
    items = line.split('\t')
    num = int(items[1])
    count += num
print "%d" % (count)
