#!/usr/bin/env python


import sys
import re
import datetime


def main():

    record_re = re.compile('([\d\.:]+) - - \[(\S+) [^"]+\] "(\w+) ([^"]+) (HTTP/[\d\.]+)" (\d+) \d+ "([^"]+)" "([^"]+)"')
    for line in sys.stdin:
        match = record_re.search(line)
        if not match:
            continue
        if match.group(6) != "200":
            continue
        try:
            date = datetime.datetime.strptime(match.group(2), "%d/%b/%Y:%H:%M:%S")
        except ValueError:
            continue
        print "%s\t%s" % (match.group(1), str(date.strftime("%H:%M:%S")))
        # sort date as string OR


if __name__ == '__main__':
    main()
