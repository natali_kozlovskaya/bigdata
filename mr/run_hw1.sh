#!/bin/bash

day=$(date -d "1 day ago" '+%Y-%m-%d')
twodaysago=$(date -d "2 day ago" '+%Y-%m-%d')
threedaysago=$(date -d "3 day ago" '+%Y-%m-%d')
fourdaysago=$(date -d "4 day ago" '+%Y-%m-%d')
fivedaysago=$(date -d "5 day ago" '+%Y-%m-%d')
sixdaysago=$(date -d "6 day ago" '+%Y-%m-%d')
sevendaysago=$(date -d "7 day ago" '+%Y-%m-%d')
eightdaysago=$(date -d "8 day ago" '+%Y-%m-%d')
ninedaysago=$(date -d "9 day ago" '+%Y-%m-%d')
tendaysago=$(date -d "10 day ago" '+%Y-%m-%d')
elevendaysago=$(date -d "11 day ago" '+%Y-%m-%d')
twelvedaysago=$(date -d "12 day ago" '+%Y-%m-%d')
thirtdaysago=$(date -d "13 day ago" '+%Y-%m-%d')
fourtdaysago=$(date -d "14 day ago" '+%Y-%m-%d')

mkdir /home/nkozlovskaya/hw_metrics/$day
mkdir /hdfs/user/nkozlovskaya/hw1/$day
mkdir /home/nkozlovskaya/hw3/$day


hadoop jar /opt/hadoop/hadoop-streaming.jar -D mapreduce.job.reduces=1 -files total_hits_mapper.py,total_hits_reducer.py -input /user/sandello/logs/access.log.$day  -output hw1/$day/total_hits -mapper total_hits_mapper.py  -reducer total_hits_reducer.py 

cat /hdfs/user/nkozlovskaya/hw1/$day/total_hits/part-00000 > /home/nkozlovskaya/hw_metrics/$day/total_hits.txt


hadoop jar /opt/hadoop/hadoop-streaming.jar -D mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator -D mapred.text.key.comparator.options=-k1,2 -D stream.num.map.output.key.fields=2 -D mapred.text.key.partitioner.options=-k1,1 -D mapred.job.reduces=10 -files avg_session_time_mapper.py,avg_session_time_reducer.py -input /user/sandello/logs/access.log.$day  -output hw1/$day/avg_time -mapper avg_session_time_mapper.py -partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner -reducer avg_session_time_reducer.py 

cat /hdfs/user/nkozlovskaya/hw1/$day/avg_time/* | awk '{for (i=1;i<=NF;i++) sum[i]+=$i;}; END{for (i in sum) print  sum[i];}'  > /home/nkozlovskaya/hw_metrics/$day/avg_time.txt


hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar -D mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator -D mapred.text.key.comparator.options=-k1,2 -D stream.map.output.field.separator=*  -D stream.num.map.output.key.fields=2 -D mapreduce.job.reduces=1 -files by_country_mapper.py,by_country_reducer.py,IP2LOCATION-LITE-DB1.CSV -input /user/sandello/logs/access.log.$day  -output hw1/$day/by_country -mapper by_country_mapper.py -reducer by_country_reducer.py 

cat /hdfs/user/nkozlovskaya/hw1/$day/by_country/part-00000 > /home/nkozlovskaya/hw_metrics/$day/by_country.txt


hadoop jar /opt/hadoop/hadoop-streaming.jar \
-files new_users_1_mapper.py,new_users_1_reducer.py \
-D mapreduce.job.reduces=1 \
-D mapred.text.key.partitioner.options=-k1,1  \
-D mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-D stream.map.output.field.separator=. \
-D stream.num.map.output.key.fields=4 \
-D mapred.map.output.key.field.separator=. \
-D mapred.partition.keycomparator.options=-k1,2 \
-input /user/sandello/logs/access.log.$day  \
-output hw1/preprocessing/$day \
-mapper new_users_1_mapper.py \
-reducer new_users_1_reducer.py


hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
-D mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-D mapred.text.key.comparator.options='-k1,2' \
-D stream.map.output.field.separator=-  \
-D stream.num.map.output.key.fields=2 \
-D mapreduce.job.reduces=1 \
-files new_users_mapper.py,new_users_reducer.py \
-input /user/nkozlovskaya/hw1/preprocessing/$fourtdaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$thirtdaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$twelvedaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$elevendaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$tendaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$ninedaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$eightdaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$sevendaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$sixdaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$fivedaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$fourdaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$threedaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$twodaysago/part-00000  \
-input /user/nkozlovskaya/hw1/preprocessing/$day/part-00000  \
-output hw1/$day/new_users \
-mapper new_users_mapper.py \
-reducer 'new_users_reducer.py '$day 

cat /hdfs/user/nkozlovskaya/hw1/$day/new_users/part-00000 > /home/nkozlovskaya/hw_metrics/$day/new_users.txt


spark-submit     profile_liked_three_days.py     "/user/sandello/logs/access.log."$threedaysago     "/user/sandello/logs/access.log."$twodaysago     "/user/sandello/logs/access.log."$day     --master yarn-cluster     --num-executors 2     --executor-cores 1     --executor-memory 2048m

cat /hdfs/user/nkozlovskaya/hw3/$day/part-00000 > /home/nkozlovskaya/hw3/$day/spark.txt 