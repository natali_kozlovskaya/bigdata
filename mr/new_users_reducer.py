#!/usr/bin/env python

import sys
import re
import datetime
import numpy as np


def main(last):
    new_users = 0
    previous_ip = None

    all_dates = []

    last = datetime.datetime.strftime((datetime.datetime.strptime(last, '%Y-%m-%d')), "%d/%b/%Y")

    for line in sys.stdin:
        current_ip, date = line.strip().split('\t', 1)[0].split('-', 1)

        if current_ip == previous_ip:
            all_dates.append(date)
            previous_ip = current_ip

        else:
            if (len(all_dates) == 1) and (last in all_dates):
                new_users += 1

            all_dates = []
            all_dates.append(date)
            previous_ip = current_ip

    if (len(all_dates) == 1) and (last in all_dates):
        new_users += 1

    print "%d" % (new_users)

if __name__ == '__main__':
    last = sys.argv[1]
    main(last)