# MapReduce tasks

The aim: daily calculating of a metric using MapReduce

  * [Data](#Data)
  * [Tasks](#Tasks)

## Data

In HDFS `/user/sandello/logs` webservers of social network save logs. 

```
$ hadoop fs -ls /user/sandello/logs
Found 1 items
-rw-r--r--   3 sandello sandello    5750921 2016-10-07 18:03 /user/sandello/logs/access.log.2016-10-07
```

An example of records in log:

```
195.206.123.39 - - [24/Sep/2016:12:32:53 +0400] "GET /id18222 HTTP/1.1" 200 10703 "http://bing.com/" "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36"
176.62.191.48 - - [24/Sep/2016:12:32:53 +0400] "GET /id81715 HTTP/1.1" 200 32343 "-" "OFM search_robot .*google.*  (compatible; )"
193.9.247.243 - - [24/Sep/2016:12:32:53 +0400] "GET /id79304 HTTP/1.1" 200 29671 "http://twitter.com/" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.8 (KHTML, like Gecko) Chrome/23.0.1255.0 Safari/537.8"
194.196.94.215 - - [24/Sep/2016:12:32:53 +0400] "GET / HTTP/1.1" 200 32598 "http://yandex.ru/yandsearch" "Mozilla/5.0 (Linux; U; Android 4.2.2; de-de; HTC_One/2.24.111.5 Build/JDQ39) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
176.120.29.168 - - [24/Sep/2016:12:32:56 +0400] "GET /id50192 HTTP/1.1" 200 20065 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.99 Safari/537.36"
79.132.115.64 - - [24/Sep/2016:12:32:56 +0400] "GET /id49582 HTTP/1.1" 200 28244 "-" "Mozilla/5.0 (Windows NT 5.1; U; de; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 Opera 11.00"
```

Each log is the file [access.log](http://httpd.apache.org/docs/2.0/logs.html#accesslog).
Each raw of is a record, consisted of several fileds, divided by white. String fields are in inverted commas, whereas inner inverted commas are screenes with '\'.

Each record contains the following fields:

  * ip-address of the user (`195.206.123.39`),
  * two useless fields (`-` и `-`),
  * time of query (`[24/Sep/2016:12:32:53 +0400]`),
  * string of query (`"GET /id18222 HTTP/1.1"`),
  * HTTP-code of an answer (`200`),
  * size of answer (`10703`),
  * refer (`"http://bing.com/"`),
  * browser (User-Agent; `"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36"`).

User id identifed by IP-address.

By IP-address we may receive the location. There is a file in HDFS `/user/sandello/dicts`.

In file `IP2LOCATION-LITE-DB1.CSV` there is an info about countries and ip-addresses;

Example of records:

```
# IP2LOCATION-LITE-DB1.CSV
"34643712","34644479","RU","Russian Federation"
"34644480","34644991","DE","Germany"
"34644992","34645503","US","United States"
"34645504","34646015","NL","Netherlands"
"34646016","34646271","RU","Russian Federation"
"34646272","34646527","NL","Netherlands"
```

Each record consists of several fields:

  * lower bound of the range of ip-addresses (`34643712`),
  * higher bound of the range of ip-addresses (`34644479`),
  * country's code (`RU`),
  * full name of the country(`Russian Federation`),

Encoding/decoding of ip-address (string <-> number)

Example:

```python
# lower bound
dec = 34643712
byte_0 = (dec >> 24) & 0xff  # 2
byte_1 = (dec >> 16) & 0xff  # 16
byte_2 = (dec >>  8) & 0xff  # 159
byte_3 = (dec >>  0) & 0xff  # 0
txt = ".".join(map(str, [byte_0, byte_1, byte_2, byte_3]))  # "2.16.159.0"

# higher bound
dec = 34644479
byte_0 = (dec >> 24) & 0xff  # 2
byte_1 = (dec >> 16) & 0xff  # 16
byte_2 = (dec >>  8) & 0xff  # 161
byte_3 = (dec >>  0) & 0xff  # 255
txt = ".".join(map(str, [byte_0, byte_1, byte_2, byte_3]))  # "2.16.161.255"

# reverse
txt = "2.16.160.128"
byte_0, byte_1, byte_2, byte_3 = map(int, txt.split("."))  # 2, 16, 160, 128
dec = byte_0 << 24 | byte_1 << 16 | byte_2 << 8 | byte_3 << 0  # 34644096
```

## Tasks

For calculating of objective metrics only successful (HTTP-code 200 ) queries to the website are considered.

  * `total_hits` -- total amout of hits to website

  * `average_session_time` -- average time of session in seconds (float). Session is a sequence of queries from one user with time's gap between queries (followed one by other) not more than 30 minutes

  * `users_by_country` -- the amount of users for each country 

  * `new_users` -- the amount of new users. The user is new in day X, if he visited the social network in day X, but did not visit it during X-1, X-2, ..., X-13

