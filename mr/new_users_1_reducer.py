#!/usr/bin/env python

import sys
import re
import datetime
import numpy as np


def main():
    previous_ip = None

    for line in sys.stdin:
        current_ip, date = line.split('\t', 1)

        if current_ip == previous_ip:
            pass
        else:
            print "%s\t%s" % (current_ip, date)
            previous_ip = current_ip

if __name__ == '__main__':
    main()