#!/usr/bin/env python

import sys
import re
import datetime
from collections import defaultdict, Counter


def main():

    countries = Counter()

    current_country = None
    current_ip = None
    for line in sys.stdin:
        country, ip = line.split('\t', 1)[0].split('*', 1)
        
        if current_country != country:
            countries[country]+=1
            current_ip = ip
            current_country = country
        else:
            if current_ip != ip:
                countries[country]+=1
                current_ip = ip
                current_country = country

    for k in countries.keys():
        print "%s\t%s" % (k, countries[k])


if __name__ == '__main__':
    main()
