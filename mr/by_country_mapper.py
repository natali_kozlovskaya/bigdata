#!/usr/bin/env python


import sys
import re
import datetime


def get_values(line):
    raw = line.strip().split(',')
    ip_from = int(raw[0][1:-1])
    ip_to = int(raw[1][1:-1])
    country = raw[3][1:-1]
    return ip_from, ip_to, country


def get_country(ip_num, arr_countries):
    case = 0
    left = 0
    right = len(arr_countries)
    while(left+1 < right):
        medium = (left + right) / 2 
        if ip_num > arr_countries[medium][0]:
            left = medium
        else:
            right = medium
    if ip_num <= arr_countries[left][1]:
        return arr_countries[left][2]
    else:
        return arr_countries[right][2]


def get_num(ip_str):
    byte_0, byte_1, byte_2, byte_3 = map(int, ip_str.split(".")) 
    dec = byte_0 << 24 | byte_1 << 16 | byte_2 << 8 | byte_3 << 0
    return dec


def main():

    record_re = re.compile('([\d\.:]+) - - \[(\S+) [^"]+\] "(\w+) ([^"]+) (HTTP/[\d\.]+)" (\d+) \d+ "([^"]+)" "([^"]+)"')

    with open('IP2LOCATION-LITE-DB1.CSV') as c:
        lines = c.readlines()
        arr_countries = map(get_values, lines)

    for line in sys.stdin:
        match = record_re.search(line)
        if not match:
            continue
        if match.group(6) != "200":
            continue
        ip = match.group(1)
        ip_num = get_num(ip)
        try:
            country = get_country(ip_num, arr_countries)
        except:
            # failed this function with user 146.254.164.8 - - [17/Nov/2016:01:00:28 +0400] "GET /id6585341.228.165.106 - - [17/Nov/2016:01:00:00 +0400] "GET / HTTP/1.1" 200 31508 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36"
            country = '-'
        
        print "%s\t%d" % ('*'.join([country, ip]), 1)


if __name__ == '__main__':
    main()
