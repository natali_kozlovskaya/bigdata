#!/bin/bash

day=$(date -d "1 day ago" '+%Y-%m-%d')
twodaysago=$(date -d "2 day ago" '+%Y-%m-%d')
threedaysago=$(date -d "3 day ago" '+%Y-%m-%d')

mkdir /home/nkozlovskaya/hw_metrics/$day
mkdir /hdfs/user/nkozlovskaya/hw1/$day
mkdir /home/nkozlovskaya/hw3/$day

spark-submit     profile_liked_three_days.py     "/user/sandello/logs/access.log."$threedaysago     "/user/sandello/logs/access.log."$twodaysago     "/user/sandello/logs/access.log."$day     --master yarn-cluster     --num-executors 2     --executor-cores 1     --executor-memory 2048m

cat /hdfs/user/nkozlovskaya/hw3/$day/part-00000 > /home/nkozlovskaya/hw3/$day/spark.txt 