# Spark and Spark Streaming

The aim:
 
  * daily calculating of a metric using Apache Spark; the access to results are given from local-disk
  * calculating of three metrics on streaming of events from Apache Kafka

## TASK

  * [Data](#Data)
  * [Tasks](#Tasks)


## Data

Data: logs to web-server.

From each records in log it can be selected **user** and **visited profile**: the user is defined by IP-address, the visited profile -- identificator, encoded in URI query as `idNNNNN`.

For example, on

```
195.206.123.39 - - [24/Sep/2015:12:32:53 +0400] "GET /id18222 HTTP/1.1" 200 10703 "http://bing.com/" "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36"
```

it can be said, that user **195.206.123.39** visited profile **id18222**.

Sometimes users "like" profile. In logs it corresponds to the hit with the param like=1`, e.g.

```
195.206.123.39 - - [24/Sep/2016:12:32:53 +0400] "GET /id18222?like=1 HTTP/1.1" 200 10703 "http://bing.com/" "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36"
```

it means that user **195.206.123.39** has liked the profile **id18222**.


For calculating of objective metrics successful (HTTP-code 200 in case Spark-metric) queries to the website are considered as well as unsuccessful (in case Spark Streaming-metric).

An example of unsuccessful request:

```
91.102.204.107 - - [22/Nov/2016:00:00:00 +0400] "GET /favicon.ico HTTP/1.1" 404 0 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36"
```

Consider Apache Kafka is working on `hadoop2-10:2181`. Data is being written to `bigdatashad-2016`-topic, consisted of 4 partitions.

## Tasks

For each day in logs it should be calculated the following metric:

 * `profile_liked_three_days` -- the total amount of profiles, who were liked during last three( on day X-2 as well as X-1 as well as X);

It should be also calculated the following 3 metrics from Kafka-stream:

  * `15_second_count` -- the amount of unsuccessful hits during last 15 seconds,
  * `60_second_count` -- the amount of unsuccessful hits during last 60 seconds,
  * `total_count` -- the total amount of unsuccessful hits since starting of this utility.
