#!/usr/bin/env python

from __future__ import print_function

import sys

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

from random import randint
import re
import sys
import getpass

record_re = re.compile('([\d\.:]+) - - \[(\S+) [^"]+\] "(\w+) ([^"]+) (HTTP/[\d\.]+)" (\d+) \d+ "([^"]+)" "([^"]+)"')


def update_func(new_values, last_sum):
    return sum(new_values) + (last_sum or 0)


def print_results(rdd):
    r = rdd.take(3)
    if len(r)==3:
    	print("15_second_count=%s; 60_second_count=%s; total_count=%s;" % (str(r[0]),str(r[1]), str(r[2])))
    elif len(r)==2:
        print("15_second_count=0; 60_second_count=%s; total_count=%s;" % (str(r[0]), str(r[1])))
    elif len(r)==1:
        print("15_second_count=0; 60_second_count=0; total_count=%s;" % (str(r[0])))
    else:
        print ("15_second_count=0; 60_second_count=0; total_count=0;")


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: hw_kafka.py <zk> <topic>", file=sys.stderr)
        exit(-1)

    sc = SparkContext(appName="hw_kafka")
    ssc = StreamingContext(sc, 15)
    ssc.checkpoint("checkpointKafka")

    logger = sc._jvm.org.apache.log4j
    logger.LogManager.getLogger("org"). setLevel( logger.Level.ERROR )
    logger.LogManager.getLogger("akka").setLevel( logger.Level.ERROR )

    zkQuorum, topic = sys.argv[1:]
    uniq_id = 'natali_kozlovskaya'#randint(1000,9999)

    kvs = KafkaUtils.createStream(ssc, zkQuorum, uniq_id, {topic: 4})
    lines = kvs.map(lambda x: x[1])

    total = lines.filter(lambda z: record_re.search(z).group(6)!="200").map(lambda x: ("key", 1))
    total_15 = total.reduceByKey(lambda a, b: a + b)
    total_60 = total.reduceByKeyAndWindow(lambda x, y: x + y, lambda x, y: x - y, 60, 15)
    total_all = total.updateStateByKey(update_func)
    joinedStream = total_15.union(total_60).union(total_all).map(lambda (k, v) : v)
    joinedStream.foreachRDD(print_results)

    ssc.start()
    ssc.awaitTermination()
