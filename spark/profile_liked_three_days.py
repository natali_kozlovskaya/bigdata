#!/usr/bin/env python

import sys
from pyspark import SparkContext
from pyspark import SparkConf
import re


record_re = re.compile('([\d\.:]+) - - \[(\S+) [^"]+\] "(\w+) ([^"]+) (HTTP/[\d\.]+)" (\d+) \d+ "([^"]+)" "([^"]+)"')

number = 0

if __name__ == "__main__":
    conf = SparkConf() \
        .setAppName("ExampleProfileCount") \
        .set("spark.ui.port", "4070")
    sc = SparkContext(conf=conf)

    lines_1 = sc.textFile("hdfs://hadoop2-10.yandex.ru:8020%s" % sys.argv[1])
    lines_2 = sc.textFile("hdfs://hadoop2-10.yandex.ru:8020%s" % sys.argv[2])
    lines_3 = sc.textFile("hdfs://hadoop2-10.yandex.ru:8020%s" % sys.argv[3])
    current_date = sys.argv[3].rsplit('.')[-1]
    prof_1 = lines_1.filter(lambda z: record_re.search(z).group(6)=="200") \
        .map(lambda x: record_re.search(x).group(4)).filter(lambda y: y \
            .endswith('like=1')).distinct().collect()
    prof_2 = lines_2.filter(lambda z: record_re.search(z).group(6)=="200") \
        .map(lambda x: record_re.search(x).group(4)).filter(lambda y: y \
            .endswith('like=1')).distinct().collect()
    prof_3 = lines_3.filter(lambda z: record_re.search(z).group(6)=="200") \
        .map(lambda x: record_re.search(x).group(4)).filter(lambda y: y \
            .endswith('like=1')).distinct().collect()
    inter = set.intersection(set.intersection(set(prof_1), set(prof_2)), set(prof_3))
    number = len(inter)

    sc.parallelize([number], 1).saveAsTextFile("/user/nkozlovskaya/hw3/%s" % current_date)
    sc.stop()