# HiveQL

The aim: to give a response on concrete ad-hoc business-queries

## TASK

  * [Data](#Data)
  * [Tasks](#Tasks)
  * [Description](#Description)

## Data

Data: logs to web-server.

From each records in log it can be selected **user** and **visited profile**: the user is defined by IP-address, the visited profile -- identificator, encoded in URI query as `idNNNNN`.

For example, on

```
195.206.123.39 - - [24/Sep/2015:12:32:53 +0400] "GET /id18222 HTTP/1.1" 200 10703 "http://bing.com/" "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36"
```

it can be said, that user **195.206.123.39** visited profile **id18222**.

For calculating of objective metrics only successful (HTTP-code 200) queries to the website are considered. Unsuccessful queries are ignored.

## Tasks

All tasks are considered relative to one fixed day.

1. `[earliest_hit]` 
Query: the most earliest hit over the day (with the lexicographically least IP)
Result: the row with tab-separated fields: `ip`, `date` (без таймзоны), `url`, `status_code`, `referer`, `user_agent`)
(Example: `100.43.79.10    17/Nov/2015:00:00:10    /id40601        200     -       Mozilla/5.0 (iPad; CPU OS 8_4_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) CriOS/45.0.2454.68 Mobile/12H321 Safari/600.1`)

2. `[top_10_hours]` 
Query: top-10 hours in a day with the largest amount of hits. If there are several hours with the same amount of hits, then we should choose the least one.
Result: twi tab-separated columns: an hour (two digits: 00, 01, ..., 23) and an amount of hits.

3. `[most_visited_profile_by_hour]` 
Query: the most visited profile for each hour. If there are several profiles, we should choose the lexicographically least profile.
Result: two tab-separated columns: an hour (two digits: 00, 01, ..., 23) and profile (format: `id12345`).

4. `[users_today_with_three_hits]` 
Query: the amount of users, who have visited the website today and made more than 3 hits, but had not visited the website yesterday. 

## Description

  * queries are implemented on HiveQL;
  * tables are considered to be created in database NKOZLOVSKAYA;
  * each response to the request is calculated by running hiveql-script (with the following corresponding MR-task);

  
