USE nkozlovskaya;
add jar /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;

# earliest_hit
SELECT hour, count(1) AS hits FROM (SELECT ip, from_unixtime(unix_timestamp(date ,'dd/MMM/yyyy:HH:mm:ss'), 'HH') AS hour FROM nkozlovskaya where status="200" and day='2016-12-01') s GROUP BY hour ORDER BY hits DESC LIMIT 10;

# top_10_hours
SELECT hour, id FROM (SELECT hour, ROW_NUMBER() OVER (PARTITION BY hour ORDER BY cnt DESC, id ASC) AS row_num, id FROM (SELECT hour, id, count(1) AS cnt FROM (SELECT SUBSTR(url, 2, 7) AS id, ip, from_unixtime(unix_timestamp(date ,'dd/MMM/yyyy:HH:mm:ss'), 'HH') AS hour FROM nkozlovskaya WHERE day='2016-12-01' AND status='200' AND SUBSTR(url, 1, 3)="/id") s GROUP BY hour, id ORDER BY cnt DESC) m ) t WHERE row_num = 1;

# most_visited_profile_by_hour
SELECT count(1) FROM (SELECT candidate, count(1) as cnt FROM (SELECT t1.ip as candidate FROM (select * from nkozlovskaya where status=200) t1 LEFT JOIN (select* from nkozlovskaya_2 where status=200) t2 ON t2.ip=t1.ip WHERE t2.ip IS NULL) s GROUP BY candidate) m WHERE cnt > 3 LIMIT 1;

# users_today_with_three_hits
SELECT ip, date, url, status, referer, user_agent FROM nkozlovskaya where status="200" and day='2016-12-01' ORDER BY ip ASC, date ASC LIMIT 1;