# in hive-shell

CREATE DATABASE nkozlovskaya;
USE nkozlovskaya;

CREATE EXTERNAL TABLE nkozlovskaya (
    ip STRING,
    date STRING,
    url STRING,
    status STRING,
    referer STRING,
    user_agent STRING
)
PARTITIONED BY (day string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.contrib.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
    "input.regex" = "([\\d\\.:]+) - - \\[(\\S+) [^\"]+\\] \"\\w+ ([^\"]+) HTTP/[\\d\\.]+\" (\\d+) \\d+ \"([^\"]+)\" \"(.*?)\""
)
STORED AS TEXTFILE;

ALTER TABLE nkozlovskaya ADD PARTITION(day='2016-12-01')
LOCATION '/user/agorokhov/logs/2016-12-01';