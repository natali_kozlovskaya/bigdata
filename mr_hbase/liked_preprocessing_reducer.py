#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import datetime
from collections import Counter


previous_id = None
previous_ip = None
previous_date = None 

for line in sys.stdin:
    items = line.split('\t')[0].split('#')
    
    current_id = items[0]
    current_date = items[2]
    current_ip = items[1]
    
    if current_id == previous_id:
        if current_ip == previous_ip:
            previous_date = current_date
            continue
        else:
            # ip is changed => write previous
            print "%s\t%s\t%s" % (previous_id, previous_ip, previous_date)
            previous_ip = current_ip
            previous_date = current_date
            
    else:
        # if next user is here
        if previous_id is not None:
            # write the old one
            print "%s\t%s\t%s" % (previous_id, previous_ip, previous_date)
            previous_id = current_id
            previous_ip = current_ip
            previous_date = current_date
            
        # if first iteration
        else:
            previous_id = current_id
            previous_ip = current_ip
            previous_date = current_date
        
# write last session
print "%s\t%s\t%s" % (previous_id, previous_ip, previous_date)
