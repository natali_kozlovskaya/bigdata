#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import datetime

print >> sys.stderr, 'start mapper'

for line in sys.stdin:
    items = line.strip().split('\t')
    cur_id = items[0]
    cur_ip = items[1]
    cur_date = items[2]
    print "%s#%s#%s\t%d" % (cur_id, cur_date, cur_ip, 1)