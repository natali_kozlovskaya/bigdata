#!/bin/bash

day=$(date -d "1 day ago" '+%Y-%m-%d')
twodaysago=$(date -d "2 day ago" '+%Y-%m-%d')
threedaysago=$(date -d "3 day ago" '+%Y-%m-%d')
fourdaysago=$(date -d "4 day ago" '+%Y-%m-%d')
fivedaysago=$(date -d "5 day ago" '+%Y-%m-%d')

mkdir /home/nkozlovskaya/hw_metrics/$day
mkdir /hdfs/user/nkozlovskaya/hw2/$day

# metric1

# 1
hadoop jar /opt/hadoop/hadoop-streaming.jar -D mapreduce.job.reduces=1 -files profile_hits_mapper.py,profile_hits_reducer.py -input /user/sandello/logs/access.log.$day  -output hw2/$day/profile_hits -mapper ./profile_hits_mapper.py -reducer ./profile_hits_reducer.py

# 2
cat /hdfs/user/nkozlovskaya/hw2/$day/profile_hits/part-00000 > /hdfs/user/nkozlovskaya/hw2/$day/profile_hits/p.tsv

# 3
hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.columns="HBASE_ROW_KEY,cf:"$day bigdatashad_nkozlovskaya_profile_hits /user/nkozlovskaya/hw2/$day/profile_hits/p.tsv

# metric 2

hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
-D mapreduce.job.reduces=1 \
-files profile_users_mapper.py,profile_users_reducer.py \
-input /user/sandello/logs/access.log.$day \
-output hw2/$day/profile_users \
-mapper ./profile_users_mapper.py \
-reducer ./profile_users_reducer.py \

cat /hdfs/user/nkozlovskaya/hw2/$day/profile_user/part-00000 > /hdfs/user/nkozlovskaya/hw2/$day/profile_users/p.tsv \

hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.columns="HBASE_ROW_KEY,cf:"$day bigdatashad_nkozlovskaya_profile_users /user/nkozlovskaya/hw2/$day/profile_users/p.tsv


# metric 3

hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
-D mapreduce.job.reduces=1 \
-files most_visited_mapper.py,most_visited_reducer.py \
-input /user/sandello/logs/access.log.$day \
-output hw2/$day/most_visited \
-mapper ./most_visited_mapper.py \
-reducer ./most_visited_reducer.py \

cat /hdfs/user/nkozlovskaya/hw2/$day/most_visited/part-00000 > /hdfs/user/nkozlovskaya/hw2/$day/most_visited/p.tsv \

hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.columns="HBASE_ROW_KEY,cf:"$day bigdatashad_nkozlovskaya_user_most_visited_profiles /user/nkozlovskaya/hw2/$day/most_visited/p.tsv


# metric 4

hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
-D mapreduce.job.reduces=1 \
-files liked_preprocessing_mapper.py,liked_preprocessing_reducer.py \
-input /user/sandello/logs/access.log.$day \
-output hw2/preprocessing/$day \
-mapper ./liked_preprocessing_mapper.py \
-reducer ./liked_preprocessing_reducer.py \

hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
-D mapreduce.job.reduces=1 \
-D mapred.text.key.partitioner.options=-k1,1 \
-D stream.num.map.output.key.fields=3 \
-D mapreduce.map.output.key.field.separator=# \
-D mapreduce.job.output.key.comparator.class=\
org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedComparator \
-D mapred.partitioner.class=org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-D mapreduce.partition.keycomparator.options='-k1,1 -k2,2r' \
-files liked_mapper.py,liked_reducer.py \
-input /user/nkozlovskaya/hw2/preprocessing/$fivedaysago/part-00000  \
-input /user/nkozlovskaya/hw2/preprocessing/$fourdaysago/part-00000  \
-input /user/nkozlovskaya/hw2/preprocessing/$threedaysago/part-00000  \
-input /user/nkozlovskaya/hw2/preprocessing/$twodaysago/part-00000  \
-input /user/nkozlovskaya/hw2/preprocessing/$day/part-00000  \
-output hw2/$day/liked \
-mapper ./liked_mapper.py \
-reducer ./liked_reducer.py \

cat /hdfs/user/nkozlovskaya/hw2/$day/liked/part-00000 > /hdfs/user/nkozlovskaya/hw2/$day/liked/p.tsv \

hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.columns="HBASE_ROW_KEY,cf:"$day bigdatashad_nkozlovskaya_profile_last_three_liked_user /user/nkozlovskaya/hw2/2016-11-27/liked/p.tsv 

