#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import datetime

print >> sys.stderr, 'start mapper'
count=0
record_re = re.compile('([\d\.:]+) - - \[(\S+) [^"]+\] "(\w+) ([^"]+) (HTTP/[\d\.]+)" (\d+) \d+ "([^"]+)" "([^"]+)"')

for line in sys.stdin:
    match = record_re.search(line)
    if not match:
        continue
    if match.group(6) != "200":
        continue
    if match.group(4).startswith('/id'):
        try:
            date = datetime.datetime.strptime(match.group(2), "%d/%b/%Y:%H:%M:%S")
        except ValueError:
            continue
        # to be lexicographically ordered by hour (and use both digits in hour: 01 instead 1)
        current_hour = datetime.datetime.strftime(date, "%H")

        print "%s#%s#%s\t%d" % (match.group(4)[1:8], current_hour, match.group(1), 1)
