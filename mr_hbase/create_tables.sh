# in hbase shell

create 'bigdatashad_nkozlovskaya_profile_hits','cf'
create 'bigdatashad_nkozlovskaya_profile_users','cf'
create 'bigdatashad_nkozlovskaya_user_most_visited_profiles','cf'
create 'bigdatashad_nkozlovskaya_profile_last_three_liked_user','cf'

# example query:
# get 'bigdatashad_nkozlovskaya_test', 'id10001', '2016-12-01'
# get 'bigdatashad_nkozlovskaya_profile_users', 'id10001', ['2016-12-01', '2016-11-27']
# get 'bigdatashad_nkozlovskaya_user_most_visited_profiles', 'id10001', ['2016-12-01', '2016-11-27']
