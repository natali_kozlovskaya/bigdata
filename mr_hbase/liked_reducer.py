#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from collections import Counter
import datetime

previous_id = None

top_three = []

for line in sys.stdin:
    items = line.split('\t')[0].split('#')
    
    current_id = items[0]
    current_date = items[1]
    current_ip = items[2]
    
    if current_id == previous_id and len(top_three)==3:
        # we have already found all top items
        continue
    
    if current_id == previous_id:
        if current_ip not in top_three:
            top_three.append(current_ip)
            
    else:
        # if next user is here
        if previous_id is not None:
            # write the old one
            print "%s\t%s" % (previous_id, ', '.join(top_three))
            top_three = []
            top_three.append(current_ip)
            previous_id = current_id
            
        # if first iteration
        else:
            previous_id = current_id
        
# write last session
print "%s\t%s" % (previous_id, ', '.join(top_three))
