# MapReduce tasks with storing results in Hbase

The aim: daily calculating of a metric using MapReduce and saving it to Hbase-tables
 
## Task

  * [Data](#Data)
  * [Tasks](#Tasks)
  * [Description](#Description)


## Data

Data: logs to web-server.

From each records in log it can be selected **user** and **visited profile**: the user is defined by IP-address, the visited profile -- identificator, encoded in URI query as `idNNNNN`.

For example, on

```
195.206.123.39 - - [24/Sep/2015:12:32:53 +0400] "GET /id18222 HTTP/1.1" 200 10703 "http://bing.com/" "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36"
```

it can be said, that user **195.206.123.39** visited profile **id18222**.

Sometimes users "like" profile. In logs it corresponds to the hit with the param like=1`, e.g.

```
195.206.123.39 - - [24/Sep/2016:12:32:53 +0400] "GET /id18222?like=1 HTTP/1.1" 200 10703 "http://bing.com/" "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36"
```

it means that user **195.206.123.39** has liked the profile **id18222**.

For calculating of objective metrics only successful (HTTP-code 200 ) queries to the website are considered.


## Tasks

 * `profile_hits(profile, day) -> [(hour, count)]` -- total amount of views of definite profile by all website's users (distribution on hours);
 * `profile_users(profile, day) -> [(hour, count)]` -- total amount of unique users viewing definite profile (distribution on hours).
 * `user_most_visited_profiles(user, day) -> [profile]` -- set of profiles, viewed by definite user in desciending order of amount of views (in case of the same amount of views profiles should be ordered lexicographically).
 * `profile_last_three_liked_users(profile, day) -> [user]` -- last three users during five last days, who liked this profile, ordered by descending of time of visiting ( in case of day X there should be users, who liked in days X, X-1, X-2, X-3, X-4).

## Description

  * calculated metrics are stored in tables `bigdatashad_NKOZLOVSKAYA_TABLENAME`, where `TABLENAME` -- name of table corresponding to each metric;
  * metrics are calculating during MapReduce-task (library HappyBase is used for aceess to hbase-tables).

