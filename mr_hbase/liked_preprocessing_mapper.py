#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import datetime

print >> sys.stderr, 'start mapper'
count=0
record_re = re.compile('([\d\.:]+) - - \[(\S+) [^"]+\] "(\w+) ([^"]+) (HTTP/[\d\.]+)" (\d+) \d+ "([^"]+)" "([^"]+)"')

for line in sys.stdin:
    match = record_re.search(line)
    if not match:
        continue
    if match.group(6) != "200":
        continue
    if match.group(4).endswith('like=1'):
        try:
            date = datetime.datetime.strptime(match.group(2),"%d/%b/%Y:%H:%M:%S").strftime("%m/%d/%Y:%H:%M:%S")
        except ValueError:
            continue
        # profile_id, time, ip, 1
        # month - number (28/11 < 01/12)
        print "%s#%s#%s\t%d" % (match.group(4)[1:].split('?')[0], match.group(1), date, 1)
        