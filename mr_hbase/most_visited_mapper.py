#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import datetime

print >> sys.stderr, 'start mapper'
count=0
record_re = re.compile('([\d\.:]+) - - \[(\S+) [^"]+\] "(\w+) ([^"]+) (HTTP/[\d\.]+)" (\d+) \d+ "([^"]+)" "([^"]+)"')

for line in sys.stdin:
    match = record_re.search(line)
    if not match:
        continue
    if match.group(6) != "200":
        continue
    if match.group(4).startswith('/id'):
        print "%s#%s\t%d" % (match.group(1), match.group(4)[1:8], 1)