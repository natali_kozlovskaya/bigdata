#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from collections import Counter
from operator import itemgetter

previous_ip = None
previous_id = None

id_cnt = Counter()

for line in sys.stdin:
    items = line.split('\t')[0].split('#')
    
    current_ip = items[0]
    current_id = items[1]

    
    if current_ip == previous_ip:
        id_cnt[current_id]+=1
    else:
        # if next user is here
        if previous_id is not None:
            # write the old one
            id_cnt = map(lambda key: (key, id_cnt[key]), id_cnt.keys())
            id_cnt = sorted(id_cnt, key=itemgetter(0))
            id_cnt = sorted(id_cnt, key=itemgetter(1), reverse=True)
            answer = ', '.join(map(lambda i: id_cnt[i][0], range(len(id_cnt))))
            print "%s\t%s" % (previous_ip, answer)
            
            previous_ip = current_ip
            previous_id = current_id
            id_cnt = Counter()
            id_cnt[previous_id]+=1
            
        # if first iteration
        else:
            previous_ip = current_ip
            previous_id = current_id
            id_cnt[current_id]+=1
        
# write last session
id_cnt = map(lambda key: (key, id_cnt[key]), id_cnt.keys())
id_cnt = sorted(id_cnt, key=itemgetter(0))
id_cnt = sorted(id_cnt, key=itemgetter(1), reverse=True)
answer = ', '.join(map(lambda i: id_cnt[i][0], range(len(id_cnt))))
print "%s\t%s" % (previous_ip, answer)
