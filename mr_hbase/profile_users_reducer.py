#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from collections import Counter


previous_id = None
previous_ip = None

previous_hour = None
hours_cnt = Counter()

for line in sys.stdin:
    items = line.split('#')
    
    current_id = items[0]
    current_hour = int(items[1])
    current_ip = items[2]
    
    if current_id == previous_id:
        # look at hour
        if current_hour == previous_hour:
            if current_ip == previous_ip:
                continue
            else:
                previous_ip = current_ip 
                hours_cnt[current_hour]+=1
        else:
            # make it zero
            hours_cnt[current_hour]+=1
            previous_hour = current_hour
            previous_ip = current_ip
    else:
        # if next user is here
        if previous_id is not None:
            # write the old one
            answer = ', '.join(map(lambda i: str(hours_cnt[i]), range(24)))
            print "%s\t%s" % (previous_id, answer)

            previous_id = current_id
            previous_ip = current_ip
            previous_hour = current_hour
            hours_cnt = Counter()
            hours_cnt[current_hour]+=1
            
        # if first iteration
        else:
            previous_id = current_id
            previous_ip = current_ip
            previous_hour = current_hour
            hours_cnt[current_hour]+=1
        
# write last session
answer = ', '.join(map(lambda i: str(hours_cnt[i]), range(24)))
print "%s\t%s" % (previous_id, answer)

