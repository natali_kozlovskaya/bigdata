#!/usr/bin/env python

import sys
from collections import Counter

previous_id = None
hours_cnt = Counter()

for line in sys.stdin:
    items = line.split('\t')
    
    current_id = items[0]
    current_hour = int(items[1])
    
    if current_id == previous_id:
        hours_cnt[current_hour]+=1
    else:
        # if next user is here
        if previous_id is not None:
            answer = ', '.join(map(lambda i: str(hours_cnt[i]), range(24)))
            print "%s\t%s" % (previous_id, answer)
            previous_id = current_id
            hours_cnt = Counter()
            hours_cnt[current_hour]+=1
            
        # if first iteration
        else:
            previous_id = current_id
            hours_cnt[current_hour]+=1
        
# write last session
answer = ', '.join(map(lambda i: str(hours_cnt[i]), range(24)))
print "%s\t%s" % (previous_id, answer)
